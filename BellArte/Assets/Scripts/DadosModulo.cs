﻿using UnityEngine;
using System.Collections;

public class DadosModulo : MonoBehaviour 
{
	[Tooltip("Valor da largura do módulo em metros.")]
	public float largura;

	[Tooltip("Valor do comprimento do módulo em metros.")]
	public float Comprimento;


	public Transform refDir;
	public Transform refEsq;
}
