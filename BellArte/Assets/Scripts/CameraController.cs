﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraController : MonoBehaviour {

	public bool moveCamera = false;

	private Vector2 initMouse, finalMouse;

	public void HabilitaIgnoraObjetos(){
		Camera.main.GetComponent<CameraController>().moveCamera = !Camera.main.GetComponent<CameraController>().moveCamera;
	}

	public void IgnoraObjetos(bool ignora){
		Camera.main.GetComponent<CameraController>().moveCamera = ignora;
	}
	
	// Update is called once per frame
	void Update () {
		if(moveCamera){

			if(Application.isEditor){

				if (Input.GetMouseButtonDown(0)) {
					
					RaycastHit hit;

					Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

					if (Physics.Raycast(ray, out hit)){

						var temp = hit.transform.position;

						if (hit.collider.gameObject.tag != "Esfera"){
							Debug.Log(""+hit.collider.name);
							temp.z -= 5f;
							temp.y = 2f;
						}
						if(hit.collider.gameObject.tag == "Esfera"){

							Camera.main.GetComponent<CameraController>().moveCamera = false;

							if(hit.collider.name == "esferaEsq"){
								temp.z -= 5f;
								temp.x += 2f;
								temp.y = 2f;
							}
							if(hit.collider.name == "esferaDir"){
								temp.z -= 5f;
								temp.x -= 2f;
								temp.y = 2f;
							}

						}


						if(hit.collider != null ){
							Camera.main.transform.position = temp;

							Camera.main.transform.LookAt(hit.collider.transform.position);
						}

					}
						

				}


			}


		}

	}
}
