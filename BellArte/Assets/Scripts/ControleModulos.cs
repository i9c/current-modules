﻿using UnityEngine;
using System.Collections;

public class ControleModulos : MonoBehaviour
{
	public bool direita {set; get;}
	bool editando;
	Vector3 refPos;
	Quaternion refRot;
	GameObject novoModulo;
	Vector3 posBotaoEsqInicial;
	Vector3 posBotaoDirInicial;
	Quaternion rotBotaoEsqInicial;
	Quaternion rotBotaoDirInicial;

	public GameObject bolaEsq;
	public GameObject bolaDir;

	public Transform botaoEsq;
	public Transform botaoDir;

	public Transform ancoraDir;
	public Transform ancoraEsq;

	public void AdicionaModulo (GameObject modulo)
	{
		LimpaModuloAtivo();

		if(!editando)
		{
			posBotaoDirInicial = botaoDir.position;
			posBotaoEsqInicial = botaoEsq.position;
			rotBotaoDirInicial = botaoDir.rotation;
			rotBotaoEsqInicial = botaoEsq.rotation;

			editando = true;
		}

		float largura = modulo.GetComponent<DadosModulo>().largura;
		float comprimento = modulo.GetComponent<DadosModulo>().Comprimento;

		if(direita)
		{
			bolaEsq.SetActive(false);
			refPos = botaoDir.position;
			refRot = botaoDir.rotation;
		}
		else
		{
			bolaDir.SetActive(false);
			refPos = botaoEsq.position;
			refRot = botaoEsq.rotation;
		}

		novoModulo = Instantiate(modulo, refPos, refRot) as GameObject;


		var rotationVar = transform.rotation.eulerAngles;

		var ancoraDireitaNovoModulo = novoModulo.transform.FindChild("ancoraDireita");
		var ancoraEsquerdaNovoModulo = novoModulo.transform.FindChild("ancoraEsquerda");

		var diferenca = ancoraEsquerdaNovoModulo.position - novoModulo.transform.position;

		Debug.Log(""+diferenca);

		if(direita)
		{
			novoModulo.transform.localPosition = new Vector3(ancoraDir.position.x - largura/2, novoModulo.transform.position.y, ancoraDir.position.z  - comprimento/2);
			rotationVar.y = novoModulo.transform.FindChild("ancoraEsquerda").rotation.eulerAngles.y; 
			novoModulo.transform.rotation = Quaternion.Euler(rotationVar);
            novoModulo.transform.localPosition = novoModulo.transform.localPosition - diferenca;

        }
		else
		{
			novoModulo.transform.localPosition = new Vector3(ancoraEsq.position.x + largura / 2, novoModulo.transform.position.y, ancoraEsq.position.z - comprimento / 2);
			rotationVar.y = novoModulo.transform.FindChild("ancoraDireita").rotation.eulerAngles.y - ancoraEsq.transform.rotation.eulerAngles.y;
            novoModulo.transform.rotation = Quaternion.Euler(rotationVar*-1);
		}
	}

	public void LimpaModuloAtivo ()
	{
		if(novoModulo != null)
		{
			Destroy(novoModulo);
		}
	}

	public void Cancela ()
	{
		if(novoModulo != null)
		{
			Destroy(novoModulo);
		}

		botaoDir.position = posBotaoDirInicial;
		botaoEsq.position = posBotaoEsqInicial;
		botaoDir.rotation = rotBotaoDirInicial;
		botaoEsq.rotation = rotBotaoEsqInicial;

		editando = false;
	}

	public void Confirma ()
	{

		bolaDir.SetActive(true);
		bolaEsq.SetActive(true);

		if(direita)
		{
			botaoDir.position = novoModulo.GetComponent<DadosModulo>().refDir.position;
			botaoDir.rotation = novoModulo.GetComponent<DadosModulo>().refDir.rotation;
			ancoraDir.position = novoModulo.GetComponent<DadosModulo>().refDir.position;
			ancoraDir.rotation = novoModulo.GetComponent<DadosModulo>().refDir.rotation;
		}
		else
		{
			botaoEsq.position = novoModulo.GetComponent<DadosModulo>().refEsq.position;
			botaoEsq.rotation = novoModulo.GetComponent<DadosModulo>().refEsq.rotation;
			ancoraEsq.position = novoModulo.GetComponent<DadosModulo>().refEsq.position;
			ancoraEsq.rotation= novoModulo.GetComponent<DadosModulo>().refEsq.rotation;
		}

		novoModulo = null;
		editando = false;
	}
}
